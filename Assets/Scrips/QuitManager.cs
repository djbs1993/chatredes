using UnityEngine;

public class QuitManager : MonoBehaviour
{

    public void QuitApplication()
    {
        Debug.Log("Application is quitting...");
        Application.Quit();
    }
}

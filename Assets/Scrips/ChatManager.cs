using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;
using TMPro;

public class ChatManager : NetworkBehaviour
{
    [SerializeField] private TMP_InputField messageToSendInputField;
    [SerializeField] private TMP_Text messagesTextContainer;
    [SerializeField] private UserList userList;

    private string username;

    private void Awake()
    {
        username = PlayerPrefs.GetString("username", "User " + Random.Range(0, 9999));
    }

    private void Start()
    {
        if (userList == null)
        {
            Debug.LogError("UserList is not assigned in the ChatManager script.");
        }
    }

    public void Out_SendNewMessage()
    {
        if (userList == null)
        {
            Debug.LogError("UserList is not assigned in the ChatManager script.");
            return;
        }

        SendMessageServerRpc(messageToSendInputField.text, NetworkManager.Singleton.LocalClientId);
    }

    [ServerRpc(RequireOwnership = false)]
    public void SendMessageServerRpc(string message, ulong clientId)
    {
        if (userList == null)
        {
            Debug.LogError("UserList is not assigned in the ChatManager script.");
            return;
        }

        string userWhoSendsMessage = userList.GetUsername(clientId);
        SendMessageToAllClientRpc(message, userWhoSendsMessage);
    }

    [ClientRpc]
    public void SendMessageToAllClientRpc(string message, string userWhoSendsMessage)
    {
        messagesTextContainer.text += "\n" + userWhoSendsMessage + ": " + message;
    }
}

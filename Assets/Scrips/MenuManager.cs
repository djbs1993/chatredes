using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;
using TMPro;
using Unity.Netcode.Transports.UTP;

public class MenuManager : MonoBehaviour
{
    [SerializeField] private TMP_InputField input_IPAddress;
    [SerializeField] private TMP_InputField input_Port;
    [SerializeField] private TMP_InputField input_Username;
    [SerializeField] private NetworkManager networkManager;
    private UnityTransport transport;

    private void Awake()
    {
        if (networkManager == null)
        {
            Debug.LogError("NetworkManager is not assigned in the inspector.");
        }
        else
        {
            Debug.Log("NetworkManager assigned in the inspector.");
            transport = networkManager.GetComponent<UnityTransport>();
            if (transport == null)
            {
                Debug.LogError("UnityTransport component not found on NetworkManager.");
            }
        }
    }

    private void Start()
    {
        if (networkManager != null && transport != null)
        {
            Debug.Log($"Initial Server Address: {transport.ConnectionData.Address}");
            Debug.Log($"Initial Server Port: {transport.ConnectionData.Port}");

            networkManager.OnClientConnectedCallback += OnClientConnectedCallback;
            networkManager.OnClientDisconnectCallback += OnClientDisconnectedCallback;
            networkManager.OnServerStarted += OnServerStartedCallback;
        }
    }

    public void Out_ConnectAsHost()
    {
        if (networkManager != null)
        {
            SaveUsername();
            SetHostNetworkAddress();
            networkManager.StartHost();
            LogCurrentTransportSettings();
        }
        else
        {
            Debug.LogError("NetworkManager is not assigned.");
        }
    }

    public void Out_ConnectAsClient()
    {
        if (networkManager != null && SetNetworkAddress())
        {
            SaveUsername();
            networkManager.StartClient();
            LogCurrentTransportSettings();
        }
        else
        {
            Debug.LogError("NetworkManager is not assigned or network address is not set.");
        }
    }

    public void Out_Disconnect()
    {
        if (networkManager != null)
        {
            networkManager.Shutdown();
        }
        else
        {
            Debug.LogError("NetworkManager is not assigned.");
        }
    }

    private void SaveUsername()
    {
        var username = input_Username.text;
        PlayerPrefs.SetString("username", username);
        Debug.Log($"Username set to: {username}");
    }

    private void SetHostNetworkAddress()
    {
        if (transport != null)
        {
            transport.ConnectionData.Address = "0.0.0.0";
            transport.ConnectionData.Port = 7777;
            Debug.Log($"Host IP set to {transport.ConnectionData.Address} and Port set to {transport.ConnectionData.Port}");
        }
    }

    private bool SetNetworkAddress()
    {
        if (transport != null)
        {
            var ipAddress = input_IPAddress.text;
            Debug.Log($"Attempting to set IP Address to: {ipAddress}");
            if (ushort.TryParse(input_Port.text, out ushort port))
            {
                Debug.Log($"Attempting to set Port to: {port}");
                transport.ConnectionData.Address = ipAddress;
                transport.ConnectionData.Port = port;
                Debug.Log($"Set IP to {ipAddress} and Port to {port}");
                return true;
            }
            else
            {
                Debug.LogError("Invalid port number.");
                return false;
            }
        }
        return false;
    }

    private void LogCurrentTransportSettings()
    {
        if (transport != null)
        {
            Debug.Log($"Current Transport Server Address: {transport.ConnectionData.Address}");
            Debug.Log($"Current Transport Server Port: {transport.ConnectionData.Port}");
        }
    }

    private void OnClientConnectedCallback(ulong clientId)
    {
        Debug.Log($"Client connected: {clientId}");
    }

    private void OnClientDisconnectedCallback(ulong clientId)
    {
        Debug.Log($"Client disconnected: {clientId}");
    }

    private void OnServerStartedCallback()
    {
        Debug.Log("Server started.");
    }

    private void OnDestroy()
    {
        if (networkManager != null)
        {
            networkManager.OnClientConnectedCallback -= OnClientConnectedCallback;
            networkManager.OnClientDisconnectCallback -= OnClientDisconnectedCallback;
            networkManager.OnServerStarted -= OnServerStartedCallback;
        }
    }
}

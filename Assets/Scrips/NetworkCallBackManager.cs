using UnityEngine;
using Unity.Netcode;

public class NetworkCallBackManager : MonoBehaviour
{
    [SerializeField] private UserList userList;

    void Start()
    {
        if (NetworkManager.Singleton != null)
        {
            NetworkManager.Singleton.OnServerStarted += OnServerStartedCallBack;
            NetworkManager.Singleton.OnClientConnectedCallback += OnClientConnectCallBackMethod;
            NetworkManager.Singleton.OnClientDisconnectCallback += OnClientDisconnectCallBackMethod;
            NetworkManager.Singleton.OnServerStopped += OnServerStoppedCallBack;
        }
    }

    private void OnDestroy()
    {
        if (NetworkManager.Singleton != null)
        {
            NetworkManager.Singleton.OnServerStarted -= OnServerStartedCallBack;
            NetworkManager.Singleton.OnClientConnectedCallback -= OnClientConnectCallBackMethod;
            NetworkManager.Singleton.OnClientDisconnectCallback -= OnClientDisconnectCallBackMethod;
            NetworkManager.Singleton.OnServerStopped -= OnServerStoppedCallBack;
        }
    }

    private void OnServerStartedCallBack()
    {
        Debug.Log("Server has started.");
    }

    private void OnServerStoppedCallBack(bool success)
    {
        Debug.Log("Server has stopped.");
    }

    private void OnClientDisconnectCallBackMethod(ulong clientID)
    {
        if (NetworkManager.Singleton.IsServer)
        {
            userList.RemoveUserClientRpc(clientID);
        }
    }

    private void OnClientConnectCallBackMethod(ulong clientID)
    {
        Debug.Log("Client connected: " + clientID);

        if (userList != null)
        {
            userList.UpdateUserListUI();
        }
        else
        {
            Debug.LogError("UserList is not assigned or found.");
        }
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;
using TMPro;

[Serializable]
public class UserListElement
{
    public string clientName = "";
    public ulong ClientID = 0;
}

public class UserList : NetworkBehaviour
{
    [SerializeField] private List<UserListElement> userList = new List<UserListElement>();
    [SerializeField] private TMP_Text userListText;

    public override void OnNetworkSpawn()
    {
        if (IsClient && !IsServer)
        {
            var username = PlayerPrefs.GetString("username", "User " + NetworkManager.Singleton.LocalClientId);
            NewClientConnectServerRpc(username, NetworkManager.Singleton.LocalClientId);
        }
        else if (IsServer)
        {
            var hostUsername = PlayerPrefs.GetString("username", "Host " + NetworkManager.Singleton.LocalClientId);
            NewClientConnectedClientRpc(hostUsername, NetworkManager.Singleton.LocalClientId);
        }
    }

    [ServerRpc(RequireOwnership = false)]
    public void NewClientConnectServerRpc(string nombreUsuario, ulong IdAssociated)
    {
        NewClientConnectedClientRpc(nombreUsuario, IdAssociated);

        foreach (var user in userList)
        {
            NewClientConnectedClientRpc(user.clientName, user.ClientID);
        }
    }

    [ClientRpc]
    public void NewClientConnectedClientRpc(string nombreUsuario, ulong IdAssociated)
    {
        var newUser = new UserListElement
        {
            clientName = nombreUsuario,
            ClientID = IdAssociated
        };
        if (!userList.Exists(user => user.ClientID == IdAssociated))
        {
            userList.Add(newUser);
        }
        UpdateUserListUI();
    }

    public void RemoveUser(ulong clientID)
    {
        userList.RemoveAll(user => user.ClientID == clientID);
        UpdateUserListUI();
    }

    public void UpdateUserListUI()
    {
        userListText.text = "Connected Users:\n";
        foreach (var user in userList)
        {
            userListText.text += user.clientName + "\n";
        }
    }

    public string GetUsername(ulong clientID)
    {
        var user = userList.Find(u => u.ClientID == clientID);
        return user != null ? user.clientName : "Unknown";
    }

    [ClientRpc]
    public void RemoveUserClientRpc(ulong clientID)
    {
        RemoveUser(clientID);
    }
}
